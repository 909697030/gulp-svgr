# `@proscom/gulp-svgr`

This is a [gulp](http://github.com/gulpjs/gulp) plugin which
allows you to run [svgr](https://github.com/gregberge/svgr) as a gulp task.

In current state this plugin is not generally intended for the general use
as it relies on the couple of use-case specific assumptions.

## Installation

```shell script
npm install --save-dev @proscom/gulp-svgr
# or
yarn add --dev @proscom/gulp-svgr
```

## Usage

In your `gulpfile.js` add this module as one of the transforms
applied to your files: 

```js
const gulpSvgr = require('@proscom/gulp-svgr');

function buildIcons() {
  return src('src/assets/icons/**/*.svg')
    .pipe(
      gulpSvgr({
        // You can pass any svgr options
        svgr: {
          icon: true,
          plugins: ['@svgr/plugin-jsx', '@svgr/plugin-prettier']
        },
        // To aggregate icons, pass an array (see below)
        aggregate: ['size'],
        // You can pass a function to determine which
        // icon to render when no aggregation matches props.
        // Default behavior is to render the last icon
        aggregateDefault: (name, icons) => icons[icons.length - 1],
        // Creates index.jsx file containing all the icons
        createIndex: true,
        // Or pass the filename of the index file
        createIndex: 'index.ts',
        // Icon file extension can be overridden
        extension: 'js',
        // Icon file prefix can be overridden
        // Default prefix is 'Icon'
        prefix: 'MyIcon'
      })
    )
    .pipe(dest('src/icons'));
}

module.exports.buildIcons = buildIcons;
```

### Aggregating icons

This plugin is capable of aggregating icons together.
This is useful in cases when the icon has some variability, e.g.:

```
/back/small.svg
/back/medium.svg
/back/large.svg
``` 

Then this plugin will aggregate all these icons together, so you can
dynamically choose the right variant:

```jsx
import { IconBack } from './icons';

function MyComponent() {
  return <IconBack size="small" />;   
}
```

Otherwise, you would have to import icons separately:

```jsx
import { IconBackSmall, IconBackMedium, IconBackLarge } from './icons';

function MyComponent() {
  return <IconBackSmall />;   
}
```

To aggregate icons, pass `aggregate` prop. 
It should be a one-item array
containing the name of the aggregation dimension. 
It will also be used as the prop of the resulting component
which determines which icon to use.
